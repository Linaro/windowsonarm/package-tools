import logging
from subprocess import check_call, check_output


def cygwin_path(path: str) -> str:
    return check_output(["cygpath", "-u", path]).decode().strip()


def run(*args: str):
    list_args = [*args]
    logging.info(("running command: ", list_args))
    check_call(list_args)
