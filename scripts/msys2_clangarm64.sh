#!/usr/bin/env bash

set -euo pipefail

die()
{
    echo "$@" 1>&2
    exit 1
}

kill_gpg_agent()
{
    cmd.exe /c "taskkill /IM dirmngr.exe /f" >& /dev/null || true
    cmd.exe /c "taskkill /IM gpg-agent.exe /f" >& /dev/null || true
}

add_msys_to_path()
{
    PATH=$(pwd)/msys64/usr/bin:$PATH
    export PATH
}

exec_msys_arm64()
{
    # execute as login (-l) allows to setup CLANGARM64 env
    # force through cmd.exe to nest out of current msys2 env
    export MSYSTEM=CLANGARM64
    cmd.exe /c "bash.exe -lc '$*'"
    kill_gpg_agent
}

setup_msys_arm64()
{
    if [ -f msys64/.setup ]; then
        add_msys_to_path
        return
    fi
    kill_gpg_agent

    if [ -d msys64 ]; then
        echo "delete incomplete msys64 folder"
        rm -rf msys64/
    fi

    local url="https://github.com/msys2/msys2-installer/releases/download/2023-10-26/msys2-base-x86_64-20231026.sfx.exe"
    wget -q $url --show-progress -O msys2.exe
    ./msys2.exe
    rm msys2.exe

    add_msys_to_path
    exec_msys_arm64 pacman-key --init
    exec_msys_arm64 pacman-key --populate
    exec_msys_arm64 pacman -Sy --noconfirm # do not update packages, just index
    exec_msys_arm64 pacman -S base-devel --noconfirm
    exec_msys_arm64 pacman -S mingw-w64-clang-aarch64-clang --noconfirm
    exec_msys_arm64 touch msys64/.setup
}

setup_msys_arm64
if [ $# -ge 1 ]; then
    exec_msys_arm64 "$@"
else
    exec_msys_arm64 bash
fi
