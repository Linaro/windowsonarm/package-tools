import argparse
import glob
import logging
import os
import shutil
import sys
import tempfile
from os.path import basename, dirname
from pathlib import Path

sys.path.append(dirname(dirname(__file__)))

NUGET_URL = "https://www.nuget.org/packages/"
NUGET_PACKAGES_URL = "https://www.nuget.org/api/v2/package/"
PYTHON_PACKAGE_URL = NUGET_PACKAGES_URL + "python"
PYTHONARM64_PACKAGE_URL = NUGET_PACKAGES_URL + "pythonarm64"


def parse_args():
    desc = "Cross compile a python wheel (x64 -> arm64)"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(
        "--python-version",
        required=True,
        help="Which python version (x64/arm64) to use (from nuget: " + NUGET_URL + ")",
    )
    parser.add_argument(
        "--source-dir",
        required=True,
        help="Where is code to build as a wheel",
    )
    return parser.parse_args()


def get_python(tmp_dir: Path, python_version: str) -> Path:
    from urllib.request import urlretrieve

    x64_url = PYTHON_PACKAGE_URL + "/" + python_version
    arm64_url = PYTHONARM64_PACKAGE_URL + "/" + python_version
    x64_zip = tmp_dir.joinpath("python-x64.zip")
    arm64_zip = tmp_dir.joinpath("python-arm64.zip")
    x64_dir = Path(tmp_dir.joinpath("x64"))
    arm64_dir = Path(tmp_dir.joinpath("arm64"))
    logging.info("download x64 python from: " + x64_url)
    urlretrieve(x64_url, x64_zip)
    logging.info("download arm64 python from: " + arm64_url)
    urlretrieve(arm64_url, arm64_zip)

    shutil.unpack_archive(x64_zip, x64_dir)
    shutil.unpack_archive(arm64_zip, arm64_dir)

    logging.info("move arm64 libs to x64 to enable cross compilation")
    # replace x64/tools/libs by arm64/tools/libs
    shutil.rmtree(x64_dir.joinpath("tools", "libs"))
    shutil.move(arm64_dir.joinpath("tools", "libs"), x64_dir.joinpath("tools"))
    python_exe = Path(x64_dir.joinpath("tools", "python.exe"))
    return python_exe.absolute()


def find_setup_py(source_dir: str) -> Path:
    setup_py = Path(source_dir).joinpath("setup.py")
    if not setup_py.exists():
        raise AssertionError(
            "source dir " + source_dir + " does not contain setup.py script"
        )
    return setup_py.absolute()


def prepare_python_env(python_exe: str):
    from command import run

    run(
        python_exe,
        "-m",
        "pip",
        "install",
        "pip",
        "setuptools==62.0",
        "cython",
        "wheel",
    )


def do_build_and_wheel(python_exe: str, setup_py: Path):
    from command import run

    enable_cross_env = "VSCMD_ARG_TGT_ARCH"
    os.environ[enable_cross_env] = "arm64"
    logging.info(
        "set env var "
        + enable_cross_env
        + " to "
        + os.environ[enable_cross_env]
        + " - enable cross compilation"
    )

    # some setup.py expect to be run only from their directory
    current_dir = os.getcwd()
    os.chdir(dirname(setup_py))

    # run(python_exe, basename(setup_py), "build_ext") # create wheel directly
    run(python_exe, basename(setup_py), "bdist_wheel")

    os.chdir(current_dir)


def find_wheel(source_dir: str):
    dist = Path(source_dir).joinpath("dist")
    # mypy does not know about root_dir for glob :(
    found = glob.glob("*win_arm64.whl", root_dir=dist)  # type: ignore
    if not found:
        raise AssertionError("no wheel created in " + str(dist))

    if len(found) > 2:
        raise AssertionError("found several wheels in " + str(dist) + ":" + str(found))

    return Path(dist).joinpath(found[0])


def patch_wheel(wheel: str):
    # in theory, set SETUPTOOLS_EXT_SUFFIX=.cp310-win_arm64.pyd should to
    # change name of .pyd files when building wheel.
    # Alas, it does not seems to work on some projects.
    # So do it ourselves.
    logging.info("replace occurrences to win_amd64 by win_arm64 in wheel")
    with tempfile.TemporaryDirectory() as tmp_dir:
        wheel_zip = Path(tmp_dir).joinpath("wheel.zip")
        shutil.copyfile(wheel, wheel_zip)
        content = Path(tmp_dir).joinpath("wheel")
        shutil.unpack_archive(wheel_zip, content, format="zip")
        os.remove(wheel_zip)
        pyd = glob.glob(str(content) + "/**/*win_amd64.pyd", recursive=True)
        for file in pyd:
            orig = file
            dest = file.replace("win_amd64", "win_arm64")
            logging.info("patch :" + orig + " -> " + dest)
            os.rename(orig, dest)
        shutil.make_archive(str(content), "zip", root_dir=content)
        shutil.move(wheel_zip, wheel)
        logging.info("patched wheel " + str(wheel))


def main():
    logging.getLogger().setLevel(logging.INFO)
    args = parse_args()
    setup_py = find_setup_py(args.source_dir)

    with tempfile.TemporaryDirectory() as tmp_dir:
        python_exe = get_python(Path(tmp_dir), args.python_version)
        prepare_python_env(python_exe)
        do_build_and_wheel(python_exe, setup_py)

    wheel = find_wheel(args.source_dir)
    logging.info("found wheel: " + str(wheel))
    patch_wheel(wheel)


if __name__ == "__main__":
    main()
